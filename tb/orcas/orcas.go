package main

import (
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// The structure of our testbed
type Testbed struct {
	Infraserver *tb.Resource
	XSpine      *tb.Resource
	ISpine      *tb.Resource
	ILeaf       []*tb.Resource
	XLeaf       []*tb.Resource
	Servers     []*tb.Resource
	Cables      []*hw.Cable
}

func main() {

	// Define the components of the testbed
	t := &Testbed{
		Infraserver: tb.LargeServer("infra", "04:70:00:01:99:99", ""),
		// name, BGP address, radix, gbps
		ISpine: tb.GenericFabric(tb.InfraSwitch, tb.Spine, "ispine", "10.99.0.1", 3, 100),
		// name, BGP address, radix, gbps
		XSpine: tb.GenericFabric(tb.XpSwitch, tb.Spine, "xspine", "10.99.1.1", 7, 100),
		ILeaf: []*tb.Resource{
			// name, radix, gbps
			tb.GenericLeaf(tb.InfraSwitch, "ileaf0", 5, 100),
			tb.GenericLeaf(tb.InfraSwitch, "ileaf1", 5, 100),
		},
		XLeaf: []*tb.Resource{
			// name, radix, gbps
			tb.GenericLeaf(tb.XpSwitch, "xleaf0", 5, 100),
			tb.GenericLeaf(tb.XpSwitch, "xleaf1", 5, 100),
		},
		Servers: []*tb.Resource{
			// name, infranet mac, experiment net mac
			tb.SmallServer("n0", "04:70:00:01:70:1A", "04:70:11:01:70:1A"),
			tb.MediumServer("n1", "04:70:00:01:70:1C", "04:70:11:01:70:1C"),
			tb.MediumServer("n2", "04:70:00:01:70:11", "04:70:11:01:70:11"),
			tb.LargeServer("n3", "04:70:00:01:70:1D", "04:70:11:01:70:1D"),
		},
	}
	t.Infraserver.Roles = []tb.Role{}

	// Interconnect the components of the testbed

	// infraserver - ispine
	t.Cable().Connect(t.ISpine.Swp(0), t.Infraserver.Eth(0))

	// ileaf - ispine
	t.Cable().Connect(t.ISpine.Swp(1), t.ILeaf[0].Swp(4))
	t.Cable().Connect(t.ISpine.Swp(2), t.ILeaf[1].Swp(4))

	// xleaf - xspine
	t.Cable().Connect(t.XSpine.Swp(0), t.XLeaf[0].Swp(4))
	t.Cable().Connect(t.XSpine.Swp(1), t.XLeaf[1].Swp(4))

	// server - ileaf
	t.Cable().Connect(t.Servers[0].Eth(1), t.ILeaf[0].Swp(0))
	t.Cable().Connect(t.Servers[1].Eth(1), t.ILeaf[0].Swp(1))
	t.Cable().Connect(t.Servers[2].Eth(1), t.ILeaf[0].Swp(2))
	t.Cable().Connect(t.Servers[3].Eth(1), t.ILeaf[0].Swp(3))

	// server - xleaf
	t.Cable().Connect(t.Servers[0].Eth(2), t.XLeaf[0].Swp(0))
	t.Cable().Connect(t.Servers[1].Eth(2), t.XLeaf[0].Swp(1))
	t.Cable().Connect(t.Servers[2].Eth(2), t.XLeaf[0].Swp(2))
	t.Cable().Connect(t.Servers[3].Eth(2), t.XLeaf[0].Swp(3))

	// Generate an XIR topology and show a connectivity table on the command line.
	tbxir := tb.ToXir(t, "Orcas")
	tbxir.Table()

	// Write the XIR to a JSON file
	tbxir.ToFile("tbxir.json")

}

// Implement the components interface to automatically generate XIR (tb.ToXir)
func (t *Testbed) Components() ([]*tb.Resource, []*hw.Cable) {

	rs := []*tb.Resource{t.XSpine, t.ISpine, t.Infraserver}
	rs = append(rs, t.ILeaf...)
	rs = append(rs, t.XLeaf...)
	rs = append(rs, t.Servers...)

	return rs, t.Cables

}

// Cable creates a new cable and add it to the testbed
func (t *Testbed) Cable() *hw.Cable {

	cable := hw.GenericCable(hw.Gbps(100))
	t.Cables = append(t.Cables, cable)
	return cable

}
