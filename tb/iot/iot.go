package main

import (
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/build"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

type Testbed struct {
	Pis     []*tb.Resource
	Minnows []*tb.Resource
	Xbee    *tb.Resource
	Cables  []*hw.Cable
}

func (t *Testbed) Net() *xir.Net {

	net := xir.NewNet("iot")

	for _, x := range t.Pis {
		net.AddNode(x.Node())
	}

	for _, x := range t.Minnows {
		net.AddNode(x.Node())
	}

	net.AddNode(t.Xbee.Node())

	for _, x := range t.Cables {
		net.AddLink(x.Link())
	}

	return net

}

func main() {

	t := &Testbed{
		Pis: []*tb.Resource{
			RPi4("pi0", "00:02:00:00:00:01", "00:02:00:00:00:02"),
			RPi4("pi1", "00:02:00:00:00:11", "00:02:00:00:00:12"),
			RPi4("pi2", "00:02:00:00:00:21", "00:02:00:00:00:22"),
			RPi4("pi3", "00:02:00:00:00:31", "00:02:00:00:00:32"),
		},
		Minnows: []*tb.Resource{
			Minnow("minnow0", "00:03:00:00:00:01", "00:03:00:00:00:02"),
			Minnow("minnow1", "00:03:00:00:00:11", "00:03:00:00:00:12"),
			Minnow("minnow2", "00:03:00:00:00:21", "00:03:00:00:00:22"),
			Minnow("minnow3", "00:03:00:00:00:31", "00:03:00:00:00:32"),
		},
		Xbee: XHub("xbee"),
	}

	for i, pi := range t.Pis {
		t.XBeeLink().Connect(pi.Nic(1).Ports[0], t.Xbee.Nic(1).Ports[i])
	}
	for i, mn := range t.Minnows {
		t.Cable().Connect(mn.Nic(0).Ports[1], t.Xbee.Nic(0).Ports[i])
	}

	net := t.Net()

	build.Run(net)

}

// A customized Raspberry Pi with an xbee port
func RPi4(name, imac, xmac string) *tb.Resource {

	pi := &hw.Device{
		Base: hw.Base{
			Manufacturer: "Raspberry Pi",
			Model:        "Raspberry Pi 4",
			SKU:          "PI4",
		},
		Procs:  []hw.Proc{{Cores: 4}},
		Memory: []hw.Dimm{{Capacity: hw.Gb(4)}},
		Nics: []hw.Nic{
			{
				Kind: "eth",
				Ports: hw.Ports(1, &hw.Port{
					Protocols: []hw.Layer1{hw.Base1000T},
					Capacity:  hw.Gbps(1),
				}),
			},
			{
				Kind: "xb",
				Ports: hw.Ports(1, &hw.Port{
					Protocols: []hw.Layer1{hw.XBee},
					Capacity:  hw.Gbps(1),
				}),
			},
		},
	}
	pi.Nics[0].Ports[0].Mac = imac
	pi.Nics[1].Ports[0].Mac = xmac

	return &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: sys.NewSystem(name, pi),
		LinkRoles: map[string]tb.Role{
			"eth0": tb.InfraLink,
			"xb0":  tb.XpLink,
		},
	}

}

func Minnow(name, imac, xmac string) *tb.Resource {

	x := &hw.Device{
		Base: hw.Base{
			Manufacturer: "Silicom",
			Model:        "MinnowBoard",
			SKU:          "Minnowboard",
		},
		Procs:  []hw.Proc{{Cores: 4}},
		Memory: []hw.Dimm{{Capacity: hw.Gb(2)}},
		Nics: []hw.Nic{
			{
				Kind: "eth",
				Ports: hw.Ports(2, &hw.Port{
					Protocols: []hw.Layer1{hw.Base1000T},
					Capacity:  hw.Mbps(10),
				}),
			},
		},
	}
	x.Nics[0].Ports[0].Mac = imac
	x.Nics[0].Ports[1].Mac = xmac

	return &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: sys.NewSystem(name, x),
		LinkRoles: map[string]tb.Role{
			"eth0": tb.InfraLink,
			"eth1": tb.XpLink,
		},
	}

}

func XHub(name string) *tb.Resource {

	x := &hw.Device{
		Base: hw.Base{
			Manufacturer: "Digi",
			Model:        "XBeeHub",
			SKU:          "XBH",
		},
		Procs:  []hw.Proc{{Cores: 1}},
		Memory: []hw.Dimm{{Capacity: hw.Gb(2)}},
		Nics: []hw.Nic{
			{
				Kind: "eth",
				Ports: hw.Ports(4, &hw.Port{
					Protocols: []hw.Layer1{hw.Base1000T},
					Capacity:  hw.Gbps(1),
				}),
			},
			{
				Kind: "xb",
				Ports: hw.Ports(4, &hw.Port{
					Protocols: []hw.Layer1{hw.XBee},
					Capacity:  hw.Mbps(10),
				}),
			},
		},
	}

	return &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: sys.NewSystem(name, x),
		LinkRoles: map[string]tb.Role{
			"eth0": tb.XpLink,
			"eth1": tb.XpLink,
			"eth2": tb.XpLink,
			"eth3": tb.XpLink,
			"xb0":  tb.XpLink,
			"xb1":  tb.XpLink,
			"xb2":  tb.XpLink,
			"xb3":  tb.XpLink,
		},
	}

}

func (t *Testbed) Cable() *hw.Cable {

	cable := hw.GenericCable(hw.Gbps(1))
	t.Cables = append(t.Cables, cable)

	return cable

}

//TODO we lack abstractions for wireless links
func (t *Testbed) XBeeLink() *hw.Cable {

	cable := hw.GenericCable(hw.Mbps(10))
	t.Cables = append(t.Cables, cable)

	return cable

}
