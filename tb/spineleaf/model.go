package main

import (
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

type Testbed struct {
	Gateway *tb.Resource
	XSpine  *tb.Resource
	ISpine  *tb.Resource
	ILeaf   []*tb.Resource
	XLeaf   []*tb.Resource
	Servers []*tb.Resource
	Cables  []*hw.Cable
}

func main() {

	t := &Testbed{
		// testbed gateway
		Gateway: Switch(tb.Gateway, tb.Gateway, "gateway", 2, hw.Gbps(100)),

		// infrastructure network
		ISpine: Switch(tb.InfraSwitch, tb.Spine, "ispine", 2, hw.Gbps(100)),
		ILeaf: []*tb.Resource{
			Switch(tb.InfraSwitch, tb.Leaf, "ileaf0", 5, hw.Gbps(100)),
			Switch(tb.InfraSwitch, tb.Leaf, "ileaf1", 5, hw.Gbps(100)),
		},

		// experiment netwwork
		XSpine: Switch(tb.XpSwitch, tb.Spine, "xspine", 5, hw.Gbps(100)),
		XLeaf: []*tb.Resource{
			Switch(tb.XpSwitch, tb.Leaf, "xleaf0", 5, hw.Gbps(100)),
			Switch(tb.XpSwitch, tb.Leaf, "xleaf1", 5, hw.Gbps(100)),
			Switch(tb.XpSwitch, tb.Leaf, "xleaf2", 5, hw.Gbps(100)),
			Switch(tb.XpSwitch, tb.Leaf, "xleaf3", 5, hw.Gbps(100)),
		},

		// nodes
		Servers: []*tb.Resource{
			// rack1
			Small("n0"), Small("n1"), Medium("n2"), Large("n3"),
			// rack2
			Small("n4"), Small("n5"), Medium("n6"), Large("n7"),
		},
	}

	// xspine - gateway
	t.Cable().Connect(swp(t.XSpine, 4), swp(t.Gateway, 0))

	// ileaf - ispine
	for i, leaf := range t.ILeaf {
		t.Cable().Connect(swp(t.ISpine, i), swp(leaf, 4))
	}
	// xleaf - xspine
	for i, leaf := range t.XLeaf {
		t.Cable().Connect(swp(t.XSpine, i), swp(leaf, 4))
	}
	// server - ileaf
	for i, s := range t.Servers {
		t.Cable().Connect(eth(s, 1), swp(t.ILeaf[i/4], i%4))
	}
	// server - xleaf
	for i, s := range t.Servers {
		t.Cable().Connect(eth(s, 2), swp(t.XLeaf[i/4*2], i%4))
		t.Cable().Connect(eth(s, 3), swp(t.XLeaf[i/4*2+1], i%4))
	}

	// print testbed to stdout table
	tbxir := tb.ToXir(t, "spineleaf")
	tbxir.Table()

	// save testbed xir
	tbxir.ToFile("spine-leaf.json")

}

func Switch(kind, role tb.Role, name string, radix int, gbps uint64) *tb.Resource {

	system := sys.NewSystem(name, hw.GenericSwitch(radix, gbps))
	system.Props["color"] = SwitchColor(kind, role)
	rs := &tb.Resource{
		Roles:  []tb.Role{kind, role},
		System: system,
		Alloc:  SwitchAllocModes(kind),
	}

	return rs

}

func SwitchColor(kind, role tb.Role) string {

	switch kind {
	case tb.InfraSwitch:
		switch role {
		case tb.Spine:
			return ISpineColor
		case tb.Fabric:
			return IFabricColor
		case tb.Leaf:
			return ILeafColor
		}
	case tb.XpSwitch:
		switch role {
		case tb.Spine:
			return XSpineColor
		case tb.Fabric:
			return XFabricColor
		case tb.Leaf:
			return XLeafColor
		}
	}

	return ""

}

func SwitchAllocModes(kind tb.Role) []tb.AllocMode {

	switch kind {
	case tb.InfraSwitch:
		return []tb.AllocMode{tb.NoAlloc}
	case tb.XpSwitch:
		return []tb.AllocMode{tb.NetAlloc}
	}

	return []tb.AllocMode{tb.AllocUnspec}

}

func Small(name string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 8},
	}

	for i := 0; i < 4; i++ {
		s.Memory = append(s.Memory, hw.Dimm{Capacity: hw.Gb(8)})
	}

	system := sys.NewSystem(name, s)
	system.Props["color"] = "#cacaca"

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: system,
	}

	rs.System.Device.Model = "Small Server"

	return rs

}

func Medium(name string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 8},
		{Cores: 8},
	}

	for i := 0; i < 8; i++ {
		s.Memory = append(s.Memory, hw.Dimm{Capacity: hw.Gb(16)})
	}

	system := sys.NewSystem(name, s)
	system.Props["color"] = "#747474"

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: system,
	}

	rs.System.Device.Model = "Medium Server"

	return rs

}

func Large(name string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 16},
		{Cores: 16},
	}

	for i := 0; i < 8; i++ {
		s.Memory = append(s.Memory, hw.Dimm{Capacity: hw.Gb(32)})
	}

	system := sys.NewSystem(name, s)
	system.Props["color"] = "#474747"

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: system,
	}

	rs.System.Device.Model = "Large Server"

	return rs

}

func (t *Testbed) Cable() *hw.Cable {

	cable := hw.GenericCable(hw.Gbps(100))
	t.Cables = append(t.Cables, cable)
	return cable

}

func nics(r *tb.Resource) []hw.Nic {
	return r.System.Device.Nics
}

func swp(r *tb.Resource, i int) *hw.Port {
	return nics(r)[1].Ports[i]
}

func eth(r *tb.Resource, i int) *hw.Port {
	return nics(r)[0].Ports[i]
}

func (t *Testbed) Components() ([]*tb.Resource, []*hw.Cable) {

	rs := []*tb.Resource{t.Gateway, t.XSpine, t.ISpine}
	rs = append(rs, t.ILeaf...)
	rs = append(rs, t.XLeaf...)
	rs = append(rs, t.Servers...)

	return rs, t.Cables

}

const (
	XSpineColor  = "#00279B"
	XFabricColor = "#003cf2"
	XLeafColor   = "#a2b8fa"

	ISpineColor  = "#009B53"
	IFabricColor = "#00f281"
	ILeafColor   = "#a2fad1"
)
