import mergexp as mx

net = mx.Topology('multi')

a = net.device('a')
b = net.device('b')
c = net.device('c')

lnk = net.connect([a, b])
a.endpoints[0].ip.addrs = ['10.0.1.1/24']
b.endpoints[0].ip.addrs = ['10.0.1.2/24']

net.connect([a, c])
a.endpoints[1].ip.addrs = ['10.0.2.1/24']
c.endpoints[0].ip.addrs = ['10.0.2.2/24']

mx.experiment(net)
