import mergexp as mx
from mergexp.unit import mbps, ms
from mergexp.net import capacity, latency

# start by creating a topology object
topo = mx.Topology('simple-routes')


# it's just python, write your own node constructors
def node(name, group):
    dev = topo.device(name)
    dev.props['group'] = group
    return dev


# node definitions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
alpha = [node(name, 1) for name in ['a0', 'a1', 'a2']]
bravo = [node(name, 2) for name in ['b0', 'b1', 'b2']]
charlie = [node(name, 3) for name in ['c0', 'c1', 'c2', 'cgw']]
routers = [node(name, 4) for name in ['r0', 'r1']]

# connectivity ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
wanlink = topo.connect(routers, capacity == mbps(1000))

# connect nodes
for i, a in enumerate(alpha):
    lnk = topo.connect(
        [a, routers[0]], capacity == mbps(100), latency == ms(10))
    lnk[a].ip.addrs = ['10.0.%d.10/24' % i]
    lnk[routers[0]].ip.addrs = ['10.0.%d.1/24' % i]

for i, b in enumerate(bravo):
    lnk = topo.connect(
        [b, routers[1]], capacity == mbps(5), latency == ms(80))
    lnk[b].ip.addrs = ['10.1.%d.10/24' % i]
    lnk[routers[1]].ip.addrs = ['10.1.%d.1/24' % i]

# connect nodes in 'LAN'
lnk = topo.connect(charlie, capacity == mbps(1000), latency == ms(1))

# gateway is special
charlie[3].props['color'] = '#f00'
dmz = topo.connect(
    [charlie[3], routers[1]],
    capacity == mbps(100),
    latency == ms(10)
)

# charlie network
for i, x in enumerate(charlie, 2):
    x.endpoints[0].ip.addrs = ['10.2.0.%d/24' % i]

# routers
wanlink[routers[0]].ip.addrs = ['2.0.0.1/24']
wanlink[routers[1]].ip.addrs = ['2.0.0.2/24']

# dmz
dmz[charlie[3]].ip.addrs = ['1.0.0.1/24']
dmz[routers[1]].ip.addrs = ['1.0.0.2/24']


# package up to send to merge
mx.experiment(topo)
