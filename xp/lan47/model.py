import mergexp as mx

net = mx.Topology('alpha')

nodes = [net.device('x%d'%i) for i in range(47)]
lan = net.connect(nodes)

for i,e in enumerate(lan.endpoints, 1):
    e.ip.addrs = ['10.0.0.%d/24'%i]
    e.ip.mtu = 9000

mx.experiment(net)
