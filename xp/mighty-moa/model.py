import mergexp as mx
from mergexp.meta import kind
from mergexp.unit import mbps, us
from mergexp.net import capacity, latency
from mergexp.moa import switch

topo = mx.Topology("mighty-moa")

# define 3 nodes
(a,b,c) = [topo.device(name) for name in ['a', 'b', 'c']]

# create a virtual switch implemented by moa
leaf = topo.device('leaf', kind == switch)

# interconnect the 3 nodes through the virtual switch
topo.connect([a,leaf], capacity == mbps(100),  latency == us(10))
topo.connect([b,leaf], capacity == mbps(100),  latency == us(10))
topo.connect([c,leaf], capacity == mbps(1000), latency == us(20))

mx.experiment(topo)
