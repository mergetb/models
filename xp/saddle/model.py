import mergexp as mx

net = mx.Topology('saddle')

a = [net.device('a%d'%i) for i in range(3)]
b = [net.device('b%d'%i) for i in range(3)]
net_a = net.connect(a)
net_b = net.connect(b)

s = net.device('s')

for i,e in enumerate(net_a.endpoints, 2):
    e.ip.addrs = ['10.0.1.%d/24'%i]
    e.ip.mtu = 9000

for i,e in enumerate(net_b.endpoints, 2):
    e.ip.addrs = ['10.0.2.%d/24'%i]
    e.ip.mtu = 9000

s_a = net.connect([s, a[0]])
s_b = net.connect([s, b[0]])

s_a[s].ip.addrs = ['10.0.3.1/24']
s_a[a[0]].ip.addrs = ['10.0.3.2/24']

s_b[s].ip.addrs = ['10.0.4.1/24']
s_b[b[0]].ip.addrs = ['10.0.4.2/24']

mx.experiment(net)
