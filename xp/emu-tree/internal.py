import mergexp as mx
from mergexp.meta import kind
from mergexp.net  import capacity, latency
from mergexp.unit import gbps, us, gb, ms
from mergexp.moa  import switch
from mergexp.mime import (
    mime, data, compute, src, dest, compute, listen
)

topo = mx.Topology('emutree-internal')

def t1(name):
    return topo.device(
        name,
        kind == mime,
        data == gb(1),
        compute == ms(50),
        src == "10.0.0.1",
        dest == "10.0.0.2"
    )

def t2(name):
    return topo.device(
        name,
        kind == mime,
        compute == ms(500),
        listen == "10.0.0.2"
    )

spine = topo.device('spine', kind == switch)
leaf  = [topo.device('leaf%d'%i,  kind == switch) for i in range(2)]
a, b  = t1('a'), t2('b')

topo.connect([leaf[0], spine], capacity == gbps(40), latency == us(10))
topo.connect([leaf[1], spine], capacity == gbps(40), latency == us(10))
topo.connect([a, leaf[0]], capacity == gbps(40), latency == us(20))
topo.connect([b, leaf[1]], capacity == gbps(40), latency == us(20))

mx.experiment(topo)
