import mergexp as mx
from mergexp.meta import kind
from mergexp.net  import capacity, latency
from mergexp.unit import gbps, us
from mergexp.moa import switch

topo = mx.Topology('emutree-local')

spine = topo.device('spine', kind == switch)
leaf  = [topo.device('leaf%d'%i,  kind == switch) for i in range(2)]
a,b   = [topo.device(name) for name in ['a', 'b']]

topo.connect([leaf[0], spine], capacity == gbps(40), latency == us(10))
topo.connect([leaf[1], spine], capacity == gbps(40), latency == us(10))
topo.connect([a, leaf[0]], capacity == gbps(40), latency == us(20))
topo.connect([b, leaf[1]], capacity == gbps(40), latency == us(20))

mx.experiment(topo)
