import mergexp as mx
from mergexp.unit import mbps, ms
from mergexp.net import capacity, latency

net = mx.Topology('triforce')

(a,b,c) = [net.device(name) for name in ['a', 'b', 'c']]

net.connect([a,b], capacity == mbps(100), latency == ms(10))
net.connect([b,c], capacity == mbps(200), latency == ms(20))
net.connect([c,a], capacity == mbps(300), latency == ms(30))

mx.experiment(net)
