'''
    Implement a basic nodes + routers topology with subnet addressing.
'''
import mergexp as mx
from ipaddress import ip_network, IPv4Interface

'''
Build a device (node/router).
'''
def node(name, group):
    n = net.device(name)
    n.props['group'] = group
    return n

'''
Take a router, a list of nodes, and a subnet and create a subnet
where nodes connect to the router and all addresses are in the 
given subnet.
'''
def make_subnet(rtr, nodes, subnet):
    # use slightly awkward python ip address classes to avoid
    # hardcoding address increments, etc.
    addrs = ip_network(subnet).hosts()
    for n in nodes:
        link = net.connect([rtr, n])
        # ip.addrs is a list, make it a list of one of the form
        # ['10.0.1.1/32'] for example.
        link[rtr].ip.addrs = [IPv4Interface(next(addrs)).with_prefixlen]
        link[n].ip.addrs = [IPv4Interface(next(addrs)).with_prefixlen]

# The network
net = mx.Topology('subnets')

# create nodes and routers.
routers = [node("router{}".format(i+1), 1) for i in range(3)]
nodes = [node("node{}".format(i+1), 2) for i in range(8)]

# build up the topology by subnets.
make_subnet(routers[0], [nodes[0]],  '10.0.1.0/24')
make_subnet(routers[0], [nodes[1]],  '10.0.2.0/24')
make_subnet(routers[1], nodes[1:4],  '10.0.3.0/24')
make_subnet(routers[1], [routers[2]], '10.0.4.0/24')
make_subnet(routers[2], nodes[4:],  '10.0.5.0/24')

# Let mx know this is our network
mx.experiment(net)
