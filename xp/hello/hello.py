import mergexp as mx

# create a topology named 'hello-world'
net = mx.Topology('hello-world')

# define two nodes hello and world
hello = net.device('hello')
world = net.device('world')

# connect hello and world
link = net.connect([hello, world])

# access endpoint by link operator
link[hello].ip.addrs = ['10.0.0.1/24']
link[world].ip.addrs = ['10.0.0.2/24']

# specify the net object as the experiment topology created by this script
mx.experiment(net)
