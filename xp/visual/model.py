import mergexp as mx
import random

# Example model showing differnent colors and shapes
# supported but the merge GUI

shapes = ['cross', 'diamond', 'triangle', 'square', 'circle']
colors = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet']
names = ['🐵', '🐱', '😀', '😍', '😲', '🐰']

net = mx.Topology('oohshiny')

lanCnt = 3
nodeCnt = 3

hub = net.device("hub")
hub.props["shape"] = "wye"
hub.props["color"] = "red"

for l in range(lanCnt):
    nodes = [net.device("{}-{}-{}".format(random.choice(names), l, n)) for n in range(nodeCnt)]
    for n in nodes:
        n.props["shape"] = random.choice(shapes)
        n.props["color"] = random.choice(colors)

    net.connect([hub] + nodes)

mx.experiment(net)
