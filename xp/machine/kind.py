import mergexp as mx
from mergexp.meta import kind
from mergexp.machine import physical

topo = mx.Topology('kind')

a = topo.device('a', kind == physical)

mx.experiment(topo)
