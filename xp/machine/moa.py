#!/usr/bin/python3
import mergexp as mx
from mergexp.meta import kind
from mergexp.moa import switch

topo = mx.Topology("moa")

leaf = topo.device('leaf', kind == switch)

mx.experiment(topo)
