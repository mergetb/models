import mergexp as mx
from mergexp.machine import cores, memory
from mergexp.unit import gb

topo = mx.Topology('device')

a = topo.device('a', cores >= 4, memory >= gb(4))

mx.experiment(topo)
