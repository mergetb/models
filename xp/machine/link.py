import mergexp as mx

net = mx.Topology('link')

a = net.device('a')
b = net.device('b')
net.connect([a, b])

mx.experiment(net)
