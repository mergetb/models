#!/usr/bin/python3
import mergexp as mx

net = mx.Topology('hello-world')

hello = net.device('hello')
world = net.device('world')

link = net.connect([hello, world])

link[hello].ip.addrs = ['10.0.0.1/24']
link[world].ip.addrs = ['10.0.0.2/24']

mx.experiment(net)
