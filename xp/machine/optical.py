import mergexp as mx
from mergexp.optical import wavelengths

net = mx.Topology('optical')

a = net.device('a')
b = net.device('b')
net.connect([a, b], wavelengths == 4)

mx.experiment(net)
