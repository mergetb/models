import mergexp as mx
from mergexp.mobile import collision, migration
from mergexp.stochastic import normal, poisson

net = mx.Topology('optical')

a = net.device('a')
b = net.device('b')
c = net.device('c')
net.connect(
    [a, b, c], 
    collision == poisson(rate=0.3), 
    migration == normal(mean=0.1, variance=0.05),
)

mx.experiment(net)
