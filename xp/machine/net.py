import mergexp as mx
from mergexp.net import capacity, latency
from mergexp.unit import ms, mbps

net = mx.Topology('link')

a = net.device('a')
b = net.device('b')
net.connect([a, b], capacity == mbps(1), latency == ms(10))

mx.experiment(net)
