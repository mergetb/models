#!/usr/bin/python3
import mergexp as mx
from mergexp.meta import kind
from mergexp.machine import cores, memory, arch, x86_64, qemu
from mergexp.unit import gb

topo = mx.Topology('device')

a = topo.device(
    'a', 
    kind == qemu,
    cores >= 4, 
    memory >= gb(4), 
    arch == x86_64
)

mx.experiment(topo)
