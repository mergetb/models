#!/bin/bash

set -e

for f in $(find . -name model.py)
  do echo $f; mergetb compile $f 1>/dev/null
done
