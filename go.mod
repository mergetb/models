module gitlab.com/mergetb/test

require (
	github.com/fatih/color v1.9.0 // indirect
	gitlab.com/mergetb/xir v0.2.10
)

go 1.13
